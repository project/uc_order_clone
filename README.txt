Adds a new "Clone" tab to the Ubercart order admin screen. When clicked it
allows the admin to clone the order into a new one. A few feautres:

* Pre-fills the customer ID from the cloned order, but allows admin to
  select/create a new customer if desired.
* Only clones the order's products by default.
* Allows admin to choose to also clone the delivery and shipping addresses
